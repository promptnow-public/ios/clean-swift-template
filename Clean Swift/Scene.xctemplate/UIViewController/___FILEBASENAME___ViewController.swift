//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class ___VARIABLE_sceneName___ViewController: BaseViewController {
    
    // MARK: - Properties
    
    var interactor: ___VARIABLE_sceneName___BusinessLogic?
    var router: (NSObjectProtocol & ___VARIABLE_sceneName___RoutingLogic & ___VARIABLE_sceneName___DataPassing)?
    
    // MARK: - IBOutlet Properties
    
    

    // MARK: - Object lifecycle
  
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        ___VARIABLE_sceneName___Configurator.shared.configure(viewController: self)
    }
  
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        ___VARIABLE_sceneName___Configurator.shared.configure(viewController: self)
    }
  
    // MARK: - View lifecycle
  
    override func viewDidLoad() {
        super.viewDidLoad()
        doSomething()
    }
  
    // MARK: - Request
  
    func doSomething() {
        let request = ___VARIABLE_sceneName___Scene.Something.Request()
        interactor?.doSomething(request: request)
    }
    
    // MARK: - Methods
    
}

protocol ___VARIABLE_sceneName___DisplayLogic: class {
    func displaySomething(viewModel: ___VARIABLE_sceneName___Scene.Something.ViewModel)
}

extension ___VARIABLE_sceneName___ViewController: ___VARIABLE_sceneName___DisplayLogic {
    
    func displaySomething(viewModel: ___VARIABLE_sceneName___Scene.Something.ViewModel) {
        
    }
    
}
